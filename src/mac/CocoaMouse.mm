/*
 The zlib/libpng License
 
 Copyright (c) 2005-2007 Phillip Castaneda (pjcast -- www.wreckedgames.com)
 
 This software is provided 'as-is', without any express or implied warranty. In no event will
 the authors be held liable for any damages arising from the use of this software.
 
 Permission is granted to anyone to use this software for any purpose, including commercial
 applications, and to alter it and redistribute it freely, subject to the following
 restrictions:
 
 1. The origin of this software must not be misrepresented; you must not claim that
 you wrote the original software. If you use this software in a product,
 an acknowledgment in the product documentation would be appreciated but is
 not required.
 
 2. Altered source versions must be plainly marked as such, and must not be
 misrepresented as being the original software.
 
 3. This notice may not be removed or altered from any source distribution.
 */
#include "mac/CocoaMouse.h"
#include "mac/CocoaInputManager.h"
#include "mac/CocoaHelpers.h"
#include "OISException.h"
#include "OISEvents.h"
#include <iostream>

using namespace OIS;

//-------------------------------------------------------------------//
CocoaMouse::CocoaMouse( InputManager* creator, bool buffered )
	: Mouse(creator->inputSystemName(), buffered, 0, creator)
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];

	CocoaInputManager *man = static_cast<CocoaInputManager*>(mCreator);
    mResponder = [[CocoaMouseView alloc] initWithFrame:[[man->_getWindow() contentView] frame]];
    if(!mResponder)
        OIS_EXCEPT( E_General, "CocoaMouseView::CocoaMouseView >> Error creating event responder" );
    
    [[man->_getWindow() contentView] addSubview:mResponder];
    [mResponder setOISMouseObj:this];
    
	static_cast<CocoaInputManager*>(mCreator)->_setMouseUsed(true);

    [pool drain];
}

CocoaMouse::~CocoaMouse()
{
	// Restore Mouse
//	CGAssociateMouseAndMouseCursorPosition(true);
	CGDisplayShowCursor(kCGDirectMainDisplay);

    if (mResponder)
    {
        [mResponder release];
        mResponder = nil;
    }
    
	static_cast<CocoaInputManager*>(mCreator)->_setMouseUsed(false);
}

void CocoaMouse::_initialize()
{
	mState.clear();
	CGAssociateMouseAndMouseCursorPosition(false);
}

void CocoaMouse::setBuffered( bool buffered )
{
	mBuffered = buffered;
}

void CocoaMouse::capture()
{
    [mResponder capture];
}

void CocoaMouse::setVisibility(bool state) {
    [mResponder setVisibility:state];
}

void CocoaMouse::hide(bool state) {
    [mResponder hide:state];
}

bool CocoaMouse::getVisibility(void) {
    return [mResponder isVisible];
}

void CocoaMouse::setMouseGrab(bool state) {
    [mResponder setMouseGrab:state];
}

void CocoaMouse::grab(bool state) {
    [mResponder grab:state];
}

void CocoaMouse::setMouseAutoGrab(bool state) {
	[mResponder setMouseAutoGrab:state];
}

bool CocoaMouse::getMouseAutoGrab(void) {
	return [mResponder isAutoGrabbed];
}

bool CocoaMouse::getMouseGrab(void) {
    return [mResponder isGrabbed];
}

@implementation CocoaMouseView

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        mTempState.clear();
        mMouseWarped = false;
        mNeedsToRegainFocus = true;
		hideMouse = true;
        grabMouse = false;
		mMoved = false;
		mouseCounter = 0;
		osCursorVisible = true;
		inited = false;
        
        // Hide OS Mouse
        [self hide:false];
        [self grab:false];

        NSRect clipRect = NSMakeRect(0.0f, 0.0f, 0.0f, 0.0f);
        clipRect = [[[self window] contentView] frame];
		
		// update frame dimensions
		mTempState.height = frame.size.height;
		mTempState.width = frame.size.width;
		mTempState.inWindow = false;
		
		oldMousePosition.x = 0;
		oldMousePosition.y = 0;
	
		// tracking area will be created by updateTracking areas
        [[self window] setAcceptsMouseMovedEvents:YES];
		[self setAutoresizesSubviews:YES];
		[NSCursor setHiddenUntilMouseMoves:NO]; // disable hidden until mouse moves
    }
    return self;
}

- (void)viewDidMoveToWindow
{
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(windowResized:) name:NSWindowDidResizeNotification object:[self window]];
}

- (void)windowResized:(NSNotification *)notification
{
	NSSize size = [[self window] frame].size;
	[self setFrameSize:size]; // updated frame size
	//NSLog(@"window width = %f, window height = %f",size.width,size.height);
}

- (void)updateTrackingAreas
{
	[self removeTrackingArea:mTrackingArea];
	[mTrackingArea release];
	// Use NSTrackingArea to track mouse move events
	// The specified options, say that:
	// NSTrackingMouseMoved - we are notified of mouse movements
	// NSTrackingMouseEnteredAndExited - we are notified if the mouse enters or exits the application
	// NSTrackingActiveInActiveApp - we only track if the window is active
	// NSTrackingInVisibleRect - we only track in visible portions of the view
	NSTrackingAreaOptions trackingOptions =
	NSTrackingMouseMoved | NSTrackingMouseEnteredAndExited | NSTrackingActiveInActiveApp | NSTrackingInVisibleRect;
	
	NSDictionary *trackerData = [NSDictionary dictionaryWithObjectsAndKeys:
								 [NSNumber numberWithInt:0], @"OISMouseTrackingKey", nil];
	mTrackingArea = [[NSTrackingArea alloc]
					 initWithRect:[self frame]// in our case track the entire view
					 options:trackingOptions
					 owner:self
					 userInfo:trackerData];
	[self addTrackingArea:mTrackingArea];
	mTempState.width  = [self frame].size.width;
	mTempState.height = [self frame].size.height;
	// std::cout << "Updated tracking area, new size["<<mTempState.width<<" x "<<mTempState.height<<"]" << std::endl;
	// check if we need to initialize the mouse
	if (!inited) {
		// move the mouse to the center of the window
		CGPoint newCursorPosition;
		NSRect sFrame = [self frame];
		NSRect rect = [[self window] convertRectToScreen:rect];
		// std::cout << "Frame size: "<<sFrame.size.width<<" x "<<sFrame.size.height<<" origin: ("<<rect.origin.x<<", "<<rect.origin.y<<")"<<std::endl;
		newCursorPosition.x = rect.origin.x + sFrame.size.width / 2.0f;
		newCursorPosition.y = [[[self window] screen] frame].size.height - (rect.origin.y + sFrame.size.height / 2.0f);
		// new do update old position
		oldMousePosition.x = mTempState.width >> 1; // center x
		oldMousePosition.y = mTempState.height >> 1; // center y
		//std::cout << "Warp to ("<<newCursorPosition.x<<", "<<newCursorPosition.y<<")" << std::endl;
		CGWarpMouseCursorPosition(newCursorPosition);
		mMouseWarped = true;
		mTempState.inWindow = true;
		mNeedsToRegainFocus = false;
		inited = true;
	}
}

- (void)dealloc
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	
    [super dealloc];
    
    [mTrackingArea release];
    mTrackingArea = 0;
}

- (BOOL)acceptsFirstMouse:(NSEvent *)theEvent
{
    return YES;
}

- (void)setOISMouseObj:(CocoaMouse *)obj
{
    oisMouseObj = obj;
}

- (void)setVisibility:(bool)state
{
    hideMouse = !state;
    [self hide:hideMouse];
}

- (void)hide:(bool)state
{
	if (state && osCursorVisible) {
		[NSCursor hide];
		CGDisplayHideCursor(kCGDirectMainDisplay);
		osCursorVisible = false;
		mouseCounter++;
		//std::cout << "[**] hide("<<(state ? "true":"false")<<") counter: "<<mouseCounter<<std::endl;
	} else if (!state && !osCursorVisible){
		[NSCursor unhide];
		CGDisplayShowCursor(kCGDirectMainDisplay);
		osCursorVisible = true;
		mouseCounter--;
		//std::cout << "[**] hide("<<(state ? "true":"false")<<") counter: "<<mouseCounter<<std::endl;
	}
}

- (void)setMouseGrab:(bool)state
{
    grabMouse = state;
    [self grab:state];
}

- (void)setMouseAutoGrab:(bool)state
{
	autoGrab = state;
}

- (bool)isAutoGrabbed
{
	return autoGrab;
}

- (void)grab:(bool)state
{
//	std::cout << "grab("<<(state?"true":"false")<<")"<<std::endl;
	CGAssociateMouseAndMouseCursorPosition(true);
/*
    if (state) {
		CGAssociateMouseAndMouseCursorPosition(false);
    } else {
        CGAssociateMouseAndMouseCursorPosition(true);
    }
*/
}

- (bool)isVisible
{
    return !hideMouse;
}

- (bool)isGrabbed
{
    return !grabMouse;
}

- (void)capture
{
	MouseState *state = oisMouseObj->getMouseStatePtr();
    state->X.rel = 0;
    state->Y.rel = 0;
    state->Z.rel = 0;
    state->inWindow = !mNeedsToRegainFocus;
	
	mMouseWarped = false;
    
	if(mMoved == true)
	{
//		NSLog(@"%i %i %i", mTempState.X.rel, mTempState.Y.rel, mTempState.Z.rel);
        
		// Set new relative motion values
		state->X.rel = mTempState.X.rel;
		state->Y.rel = mTempState.Y.rel;
		state->Z.rel = mTempState.Z.rel;
		
		// Update absolute position
		state->X.abs = mTempState.X.abs;
		state->Y.abs = mTempState.Y.abs;
		
		if(state->X.abs > state->width)
			state->X.abs = state->width;
		else if(state->X.abs < 0)
			state->X.abs = 0;
        
		if(state->Y.abs > state->height)
			state->Y.abs = state->height;
		else if(state->Y.abs < 0)
			state->Y.abs = 0;
        
		state->Z.abs += mTempState.Z.rel;
		
		//Fire off event
        if ( oisMouseObj->buffered())
        	oisMouseObj->propagateEvent(ME_Moved, OIS::MouseEvent(oisMouseObj,*state));
	
		mMoved = false;
	}
	
	// Check to see if we need to update height/width
	if (mTempState.height != [self frame].size.height || mTempState.width != [self frame].size.width) {
		//std::cout << "Frame dimensions changed! from "<<mTempState.width << " x "<< mTempState.height<<" to "<<[self frame].size.width<<" x "<<[self frame].size.height<<std::endl;
		mTempState.height = [self frame].size.height;
		mTempState.width = [self frame].size.width;
	}
	
	mTempState.clear();
	// make sure we keep the absolute values so tracking can work
	mTempState.X.abs = state->X.abs;
	mTempState.Y.abs = state->Y.abs;
	mTempState.Z.abs = state->Z.abs;
	state->width = mTempState.width;
	state->height = mTempState.height;
}

#pragma mark Left Mouse Event overrides
- (void)mouseDown:(NSEvent *)theEvent
{
    int mouseButton = MB_Left;
    NSEventType type = [theEvent type];
    MouseState *state = oisMouseObj->getMouseStatePtr();

    if(mNeedsToRegainFocus)
        return;
	
	if (autoGrab)
		grabMouse = true;

    if((type == NSLeftMouseDown) && ([theEvent modifierFlags] & NSAlternateKeyMask))
    {
        mouseButton = MB_Middle;
    }
    else if((type == NSLeftMouseDown) && ([theEvent modifierFlags] & NSControlKeyMask))
    {
        mouseButton = MB_Right;
    }
    else if(type == NSLeftMouseDown)
    {
        mouseButton = MB_Left;
    }
    state->buttons |= 1 << mouseButton;
    if ( oisMouseObj->buffered())
    	oisMouseObj->propagateEvent(ME_Pressed, MouseEvent( oisMouseObj, *state ), (MouseButtonID)mouseButton);
}

- (void)mouseUp:(NSEvent *)theEvent {
    int mouseButton = MB_Left;
    NSEventType type = [theEvent type];
    MouseState *state = oisMouseObj->getMouseStatePtr();
	
	if(mNeedsToRegainFocus)
        return;
	
	if (autoGrab)
		grabMouse = false;

    if((type == NSLeftMouseUp) && ([theEvent modifierFlags] & NSAlternateKeyMask))
    {
        mouseButton = MB_Middle;
    }
    else if((type == NSLeftMouseUp) && ([theEvent modifierFlags] & NSControlKeyMask))
    {
        mouseButton = MB_Right;
    }
    else if(type == NSLeftMouseUp)
    {
        mouseButton = MB_Left;
    }
    state->buttons &= ~(1 << mouseButton);

	if ( oisMouseObj->buffered())
    	oisMouseObj->propagateEvent(ME_Released, MouseEvent( oisMouseObj, *state ), (MouseButtonID)mouseButton);
}


- (void)mouseDragged:(NSEvent *)theEvent
{
    [self _mouseMoved:theEvent];
}

#pragma mark Right Mouse Event overrides
- (void)rightMouseDown:(NSEvent *)theEvent
{
    int mouseButton = MB_Right;
    NSEventType type = [theEvent type];
    MouseState *state = oisMouseObj->getMouseStatePtr();
    
    if(mNeedsToRegainFocus)
        return;
	
	if (autoGrab)
		grabMouse = true;
    
    if(type == NSRightMouseDown)
    {	
        state->buttons |= 1 << mouseButton;
    }

	if ( oisMouseObj->buffered())
    	oisMouseObj->propagateEvent(ME_Pressed, MouseEvent( oisMouseObj, *state ), (MouseButtonID)mouseButton);
}

- (void)rightMouseUp:(NSEvent *)theEvent {
    int mouseButton = MB_Right;
    NSEventType type = [theEvent type];
    MouseState *state = oisMouseObj->getMouseStatePtr();
	
	if(mNeedsToRegainFocus)
        return;
	
	if (autoGrab)
		grabMouse = false;
    
    if(type == NSRightMouseUp)
    {	
        state->buttons &= ~(1 << mouseButton);
    }

    if ( oisMouseObj->buffered())
    	oisMouseObj->propagateEvent(ME_Released, MouseEvent( oisMouseObj, *state ), (MouseButtonID)mouseButton);
}

- (void)rightMouseDragged:(NSEvent *)theEvent
{
    [self _mouseMoved:theEvent];
}

#pragma mark Other Mouse Event overrides
- (void)otherMouseDown:(NSEvent *)theEvent
{
    int mouseButton = MB_Middle;
    NSEventType type = [theEvent type];
    MouseState *state = oisMouseObj->getMouseStatePtr();
    
    if(mNeedsToRegainFocus)
        return;
	
	if (autoGrab)
		grabMouse = true;
    
    if(type == NSOtherMouseDown)
    {
        state->buttons |= 1 << mouseButton;
    }

    if ( oisMouseObj->buffered())
    	oisMouseObj->propagateEvent(ME_Pressed, MouseEvent( oisMouseObj, *state ), (MouseButtonID)mouseButton);
}

- (void)otherMouseUp:(NSEvent *)theEvent {
    int mouseButton = MB_Middle;
    NSEventType type = [theEvent type];
    MouseState *state = oisMouseObj->getMouseStatePtr();
	
	if(mNeedsToRegainFocus)
        return;
	
	if (autoGrab)
		grabMouse = false;
    
    if(type == NSOtherMouseUp)
    {
        state->buttons &= ~(1 << mouseButton);
    }

    if ( oisMouseObj->buffered())
    	oisMouseObj->propagateEvent(ME_Released, MouseEvent( oisMouseObj, *state ), (MouseButtonID)mouseButton);
}

- (void)otherMouseDragged:(NSEvent *)theEvent
{
    [self _mouseMoved:theEvent];
}

- (void)scrollWheel:(NSEvent *)theEvent
{
	// Pass the event to the mouse moved handler
	[self _mouseMoved:theEvent];
	
    if([theEvent deltaY] != 0.0) {
        mTempState.Z.rel += ([theEvent deltaY] * 60);
		mMoved = true; // because of the scrool action
	}
}

- (void)_mouseMoved:(NSEvent *)theEvent
{
//	CGPoint delta = CGPointMake([theEvent deltaX], [theEvent deltaY]);
    if(mNeedsToRegainFocus)
        return;
	
	NSPoint mouseLocation = [theEvent locationInWindow];
	int mPosX = (int) mouseLocation.x;
	int mPosY = (int) mTempState.height - mouseLocation.y;
	
	// std::cout << "[**] Mouse position ("<<mPosX<<", "<<mPosY<<")"<<std::endl;
	
    // Relative positioning
	// Ignore out of bounds if we just warped 
	if (mMouseWarped)
	{
		if(mPosX < 5 || mPosX > mTempState.width - 5 ||
		   mPosY < 5 || mPosY > mTempState.height - 5)
			return;
	}

	int dx = mPosX - oldMousePosition.x;
	int dy = mPosY - oldMousePosition.y;
	
	oldMousePosition.x = mPosX;
	oldMousePosition.y = mPosY;
	
	mTempState.X.abs += dx;
	mTempState.Y.abs += dy;
	mTempState.X.rel += dx;
	mTempState.Y.rel += dy;
	
	//Check to see if we are grabbing the mouse to the window (requires clipping and warping)
	if( grabMouse )
	{
		if( mTempState.X.abs < 0 )
			mTempState.X.abs = 0;
		else if( mTempState.X.abs > mTempState.width )
			mTempState.X.abs = mTempState.width;
		
		if( mTempState.Y.abs < 0 )
			mTempState.Y.abs = 0;
		else if( mTempState.Y.abs > mTempState.height )
			mTempState.Y.abs = mTempState.height;
		
		if( mNeedsToRegainFocus == false )
		{
			//Keep mouse in window (fudge factor)
			if(mPosX < 5 || mPosX > mTempState.width - 5 ||
			   mPosY < 5 || mPosY > mTempState.height - 5)
			{
				CGPoint newCursorPosition;
				NSRect sFrame = [self frame];
				NSRect rect = [[self window] convertRectToScreen:rect];
				// std::cout << "Frame size: "<<sFrame.size.width<<" x "<<sFrame.size.height<<" origin: ("<<rect.origin.x<<", "<<rect.origin.y<<")"<<std::endl;
				newCursorPosition.x = rect.origin.x + sFrame.size.width / 2.0f;
				newCursorPosition.y = [[[self window] screen] frame].size.height - (rect.origin.y + sFrame.size.height / 2.0f);
				// new do update old position
				oldMousePosition.x = mTempState.width >> 1; // center x
				oldMousePosition.y = mTempState.height >> 1; // center y
				//std::cout << "Warp to ("<<newCursorPosition.x<<", "<<newCursorPosition.y<<")" << std::endl;
				CGWarpMouseCursorPosition(newCursorPosition);
				mMouseWarped = true;
			}
		}
	} else { // if we are not grabbing, return the actual mouse position
		mTempState.X.abs = mPosX;
		mTempState.Y.abs = mPosY;
	}
	mMoved = true;
}

- (void)mouseMoved:(NSEvent *)theEvent
{
	[self _mouseMoved:theEvent];
}

- (void)mouseEntered:(NSEvent *)theEvent
{
	CGAssociateMouseAndMouseCursorPosition(true);
	mNeedsToRegainFocus = false;
    [self grab:grabMouse];
    [self hide:hideMouse];
	
	mMouseWarped = false;

//    if(!mMouseWarped)
//    {
        NSPoint pos = [[self window] mouseLocationOutsideOfEventStream];
        NSRect frame = [[[self window] contentView] frame];

        // Clear the previous mouse state
        MouseState *state = oisMouseObj->getMouseStatePtr();
        state->clear();

        // Cocoa's coordinate system has the origin in the bottom left so we need to transform the height
        mTempState.X.rel = 0;
        mTempState.Y.rel = 0;
		oldMousePosition.x = (int)pos.x;
		oldMousePosition.y = (int)frame.size.height - pos.y;
		mTempState.X.abs = oldMousePosition.x;
		mTempState.Y.abs = oldMousePosition.y;
		mTempState.inWindow = true;
		state->inWindow = true;
		mMoved = true;
 //   }
    //std::cout << "[**] Mouse entered view" << std::endl;
	//MouseState *state = oisMouseObj->getMouseStatePtr();
	if ( oisMouseObj->buffered())
    	oisMouseObj->propagateEvent(ME_Acquired, MouseEvent( oisMouseObj, *state ));
}

- (void)mouseExited:(NSEvent *)theEvent
{
	[self grab:false];
    [self hide:false];
	mNeedsToRegainFocus = true;
	CGAssociateMouseAndMouseCursorPosition(true);
	//std::cout << "[**] Mouse exited view" << std::endl;
	MouseState *state = oisMouseObj->getMouseStatePtr();
	state->inWindow = false;
	if ( oisMouseObj->buffered())
    	oisMouseObj->propagateEvent(ME_Unacquired, MouseEvent( oisMouseObj, *state ));
}

@end
