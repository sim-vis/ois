/*
 The zlib/libpng License
 
 Copyright (c) 2005-2007 Phillip Castaneda (pjcast -- www.wreckedgames.com)
 
 This software is provided 'as-is', without any express or implied warranty. In no event will
 the authors be held liable for any damages arising from the use of this software.
 
 Permission is granted to anyone to use this software for any purpose, including commercial
 applications, and to alter it and redistribute it freely, subject to the following
 restrictions:
 
 1. The origin of this software must not be misrepresented; you must not claim that
 you wrote the original software. If you use this software in a product,
 an acknowledgment in the product documentation would be appreciated but is
 not required.
 
 2. Altered source versions must be plainly marked as such, and must not be
 misrepresented as being the original software.
 
 3. This notice may not be removed or altered from any source distribution.
 */

#include "mac/CocoaKeyboard.h"
#include "mac/CocoaInputManager.h"
#include "mac/CocoaHelpers.h"
#include "OISException.h"
#include "OISEvents.h"

#include <Cocoa/Cocoa.h>

#include <list>
#include <string>
#include <iostream>

using namespace OIS;

#ifndef NX_DEVICERCTLKEYMASK
#define NX_DEVICELCTLKEYMASK    0x00000001
#endif
#ifndef NX_DEVICELSHIFTKEYMASK
#define NX_DEVICELSHIFTKEYMASK  0x00000002
#endif
#ifndef NX_DEVICERSHIFTKEYMASK
#define NX_DEVICERSHIFTKEYMASK  0x00000004
#endif
#ifndef NX_DEVICELCMDKEYMASK
#define NX_DEVICELCMDKEYMASK    0x00000008
#endif
#ifndef NX_DEVICERCMDKEYMASK
#define NX_DEVICERCMDKEYMASK    0x00000010
#endif
#ifndef NX_DEVICELALTKEYMASK
#define NX_DEVICELALTKEYMASK    0x00000020
#endif
#ifndef NX_DEVICERALTKEYMASK
#define NX_DEVICERALTKEYMASK    0x00000040
#endif
#ifndef NX_DEVICERCTLKEYMASK
#define NX_DEVICERCTLKEYMASK    0x00002000
#endif

#define OIS_USE_OSX_AS_STRING_MAP

//-------------------------------------------------------------------//
CocoaKeyboard::CocoaKeyboard( InputManager* creator, bool buffered, bool repeat )
: Keyboard(creator->inputSystemName(), buffered, 0, creator)
{
	CocoaInputManager *man = static_cast<CocoaInputManager*>(mCreator);
    mResponder = [[CocoaKeyboardView alloc] init];
    if(!mResponder)
        OIS_EXCEPT( E_General, "CocoaKeyboardView::CocoaKeyboardView >> Error creating event responder" );
	
    [man->_getWindow() makeFirstResponder:mResponder];
    [mResponder setUseRepeat:repeat];
    [mResponder setOISKeyboardObj:this];
	mModifiers = 0;
	
	static_cast<CocoaInputManager*>(mCreator)->_setKeyboardUsed(true);
}

//-------------------------------------------------------------------//
CocoaKeyboard::~CocoaKeyboard()
{
    if (mResponder)
    {
        [mResponder release];
        mResponder = nil;
    }
	
	// Free the input managers keyboard
	static_cast<CocoaInputManager*>(mCreator)->_setKeyboardUsed(false);
}

//-------------------------------------------------------------------//
void CocoaKeyboard::_initialize()
{
	mModifiers = 0;
}

void CocoaKeyboard::setKeyboardGrab(bool state)
{
	[mResponder setAutoGrab:state];
}

bool CocoaKeyboard::getKeyboardGrab(void)
{
	return [mResponder getGrabState];
}

void CocoaKeyboard::setKeyboardAutoGrab(bool state)
{
	[mResponder setAutoGrab:state];
}

bool CocoaKeyboard::getKeyboardAutoGrab(void)
{
	return [mResponder getAutoGrabState];
}



//-------------------------------------------------------------------//
bool CocoaKeyboard::isKeyDown( KeyCode key ) const
{
	return [mResponder isKeyDown:key];
}

//-------------------------------------------------------------------//
void CocoaKeyboard::capture()
{
    [mResponder capture];
}

//-------------------------------------------------------------------//
std::string& CocoaKeyboard::getAsString( KeyCode key )
{
	getString = "";
#ifdef OIS_USE_OSX_AS_STRING_MAP
	switch (key) {
		case KC_UNASSIGNED  : getString = "UNASSIGNED"; break;
		case KC_ESCAPE      : getString = "ESC"; break;
		case KC_1           : getString = "1"; break;
		case KC_2           : getString = "2"; break;
		case KC_3           : getString = "3"; break;
		case KC_4           : getString = "4"; break;
		case KC_5           : getString = "5"; break;
		case KC_6           : getString = "6"; break;
		case KC_7           : getString = "7"; break;
		case KC_8           : getString = "8"; break;
		case KC_9           : getString = "9"; break;
		case KC_0           : getString = "0"; break;
		case KC_MINUS       : getString = "-"; break;    // - on main keyboard
		case KC_EQUALS      : getString = "="; break;
		case KC_BACK        : getString = "BACK"; break;    // backspace
		case KC_TAB         : getString = "TAB"; break;
		case KC_Q           : getString = "Q"; break;
		case KC_W           : getString = "W"; break;
		case KC_E           : getString = "E"; break;
		case KC_R           : getString = "R"; break;
		case KC_T           : getString = "T"; break;
		case KC_Y           : getString = "Y"; break;
		case KC_U           : getString = "U"; break;
		case KC_I           : getString = "I"; break;
		case KC_O           : getString = "O"; break;
		case KC_P           : getString = "P"; break;
		case KC_LBRACKET    : getString = "LEFT BRACKET"; break;
		case KC_RBRACKET    : getString = "RIGHT BRACKET"; break;
		case KC_RETURN		: getString = "RETURN"; break;    // Enter on main keyboard
		case KC_LCONTROL    : getString = "LEFT CONTROL"; break;
		case KC_A           : getString = "A"; break;
		case KC_S           : getString = "S"; break;
		case KC_D           : getString = "D"; break;
		case KC_F           : getString = "F"; break;
		case KC_G           : getString = "G"; break;
		case KC_H           : getString = "H"; break;
		case KC_J           : getString = "J"; break;
		case KC_K           : getString = "K"; break;
		case KC_L           : getString = "L"; break;
		case KC_SEMICOLON   : getString = ";"; break;
		case KC_APOSTROPHE  : getString = "'"; break;
		case KC_GRAVE       : getString = "`"; break;    // accent
		case KC_LSHIFT      : getString = "LEFT SHIFT"; break;
		case KC_BACKSLASH   : getString = "\\"; break;
		case KC_Z           : getString = "Z"; break;
		case KC_X           : getString = "X"; break;
		case KC_C           : getString = "C"; break;
		case KC_V           : getString = "V"; break;
		case KC_B           : getString = "B"; break;
		case KC_N           : getString = "N"; break;
		case KC_M           : getString = "M"; break;
		case KC_COMMA       : getString = ","; break;
		case KC_PERIOD      : getString = "."; break;    // . on main keyboard
		case KC_SLASH       : getString = "/"; break;    // / on main keyboard
		case KC_RSHIFT      : getString = "RIGHT SHIFT"; break;
		case KC_MULTIPLY    : getString = "NUMPAD *"; break;    // * on numeric keypad
		case KC_LMENU       : getString = "LEFT ALT"; break;    // left Alt
		case KC_SPACE       : getString = "SPACE"; break;
		case KC_CAPITAL     : getString = "CAPS LOCK"; break;
		case KC_F1          : getString = "F1"; break;
		case KC_F2          : getString = "F2"; break;
		case KC_F3          : getString = "F3"; break;
		case KC_F4          : getString = "F4"; break;
		case KC_F5          : getString = "F5"; break;
		case KC_F6          : getString = "F6"; break;
		case KC_F7          : getString = "F7"; break;
		case KC_F8          : getString = "F8"; break;
		case KC_F9          : getString = "F9"; break;
		case KC_F10         : getString = "F10"; break;
		case KC_NUMLOCK     : getString = "NUM LOCK"; break;
		case KC_SCROLL      : getString = "SCROLL LOCK"; break;    // Scroll Lock
		case KC_NUMPAD7     : getString = "NUMPAD 7"; break;
		case KC_NUMPAD8     : getString = "NUMPAD 8"; break;
		case KC_NUMPAD9     : getString = "NUMPAD 9"; break;
		case KC_SUBTRACT    : getString = "NUMPAD -"; break;    // - on numeric keypad
		case KC_NUMPAD4     : getString = "NUMPAD 4"; break;
		case KC_NUMPAD5     : getString = "NUMPAD 5"; break;
		case KC_NUMPAD6     : getString = "NUMPAD 6"; break;
		case KC_ADD         : getString = "NUMPAD +"; break;    // + on numeric keypad
		case KC_NUMPAD1     : getString = "NUMPAD 1"; break;
		case KC_NUMPAD2     : getString = "NUMPAD 2"; break;
		case KC_NUMPAD3     : getString = "NUMPAD 3"; break;
		case KC_NUMPAD0     : getString = "NUMPAD 0"; break;
		case KC_DECIMAL     : getString = "NUMPAD ."; break;    // . on numeric keypad
		case KC_OEM_102     : getString = "OEM 102"; break;    // < > | on UK/Germany keyboards
		case KC_F11         : getString = "F11"; break;
		case KC_F12         : getString = "F12"; break;
		case KC_F13         : getString = "F13"; break;    //                     (NEC PC98)
		case KC_F14         : getString = "F14"; break;    //                     (NEC PC98)
		case KC_F15         : getString = "F15"; break;    //                     (NEC PC98)
		case KC_KANA        : getString = "KANA"; break;    // (Japanese keyboard)
		case KC_ABNT_C1     : getString = "ABNT C1"; break;    // / ? on Portugese (Brazilian) keyboards
		case KC_CONVERT     : getString = "CONVERT"; break;    // (Japanese keyboard)
		case KC_NOCONVERT   : getString = "NO CONVERT"; break;    // (Japanese keyboard)
		case KC_YEN         : getString = "YEN"; break;    // (Japanese keyboard)
		case KC_ABNT_C2     : getString = "ABNT_C2"; break;    // Numpad . on Portugese (Brazilian) keyboards
		case KC_NUMPADEQUALS: getString = "NUMPAD ="; break;    // = on numeric keypad (NEC PC98)
		case KC_PREVTRACK   : getString = "PREVIOUS TRACK"; break;    // Previous Track (KC_CIRCUMFLEX on Japanese keyboard)
		case KC_AT          : getString = "@"; break;    //                     (NEC PC98)
		case KC_COLON       : getString = ":"; break;    //                     (NEC PC98)
		case KC_UNDERLINE   : getString = "_"; break;    //                     (NEC PC98)
		case KC_KANJI       : getString = "KANJI"; break;    // (Japanese keyboard)
		case KC_STOP        : getString = "STOP"; break;    //                     (NEC PC98)
		case KC_AX          : getString = "AX"; break;    //                     (Japan AX)
		case KC_UNLABELED   : getString = "UNLABELED"; break;    //                        (J3100)
		case KC_NEXTTRACK   : getString = "NEXT TRACK"; break;    // Next Track
		case KC_NUMPADENTER : getString = "NUMPAD ENTER"; break;    // Enter on numeric keypad
		case KC_RCONTROL    : getString = "RIGHT CONTROL"; break;
		case KC_MUTE        : getString = "MUTE"; break;    // Mute
		case KC_CALCULATOR  : getString = "CALCULATOR"; break;    // Calculator
		case KC_PLAYPAUSE   : getString = "PLAY/PAUSE"; break;    // Play / Pause
		case KC_MEDIASTOP   : getString = "MEDIA STOP"; break;    // Media Stop
		case KC_VOLUMEDOWN  : getString = "VOLUME -"; break;    // Volume -
		case KC_VOLUMEUP    : getString = "VOLUME +"; break;    // Volume +
		case KC_WEBHOME     : getString = "WEB HOME"; break;    // Web home
		case KC_NUMPADCOMMA : getString = "NUMPAD ,"; break; // on numeric keypad (NEC PC98)
		case KC_DIVIDE      : getString = "NUMPAD /"; break;    // / on numeric keypad
		case KC_SYSRQ       : getString = "SYSTEM RQ"; break;
		case KC_RMENU       : getString = "RIGHT ALT"; break;    // right Alt
		case KC_PAUSE       : getString = "PAUSE"; break;    // Pause
		case KC_HOME        : getString = "HOME"; break;    // Home on arrow keypad
		case KC_UP          : getString = "UP"; break;    // UpArrow on arrow keypad
		case KC_PGUP        : getString = "PAGE UP"; break;    // PgUp on arrow keypad
		case KC_LEFT        : getString = "LEFT"; break;    // LeftArrow on arrow keypad
		case KC_RIGHT       : getString = "RIGHT"; break;    // RightArrow on arrow keypad
		case KC_END         : getString = "END"; break;    // End on arrow keypad
		case KC_DOWN        : getString = "DOWN"; break;    // DownArrow on arrow keypad
		case KC_PGDOWN      : getString = "PAGE DOWN"; break;    // PgDn on arrow keypad
		case KC_INSERT      : getString = "INSERT"; break;    // Insert on arrow keypad
		case KC_DELETE      : getString = "DELETE"; break;    // Delete on arrow keypad
		case KC_LWIN        : getString = "LEFT APPLE"; break;    // Left Windows key
		case KC_RWIN        : getString = "RIGHT APPLE"; break;    // Right Windows key
		case KC_APPS        : getString = "APPS MENU"; break;    // AppMenu key
		case KC_POWER       : getString = "POWER"; break;    // System Power
		case KC_SLEEP       : getString = "SLEEP"; break;    // System Sleep
		case KC_WAKE        : getString = "WAKE"; break;    // System Wake
		case KC_WEBSEARCH   : getString = "WEB SEARCH"; break;    // Web Search
		case KC_WEBFAVORITES: getString = "WEB FAVORITES"; break;    // Web Favorites
		case KC_WEBREFRESH  : getString = "WEB REFRESH"; break;    // Web Refresh
		case KC_WEBSTOP     : getString = "WEB STOP"; break;    // Web Stop
		case KC_WEBFORWARD  : getString = "WEB FORWARD"; break;    // Web Forward
		case KC_WEBBACK     : getString = "WEB BACK"; break;    // Web Back
		case KC_MYCOMPUTER  : getString = "MY COMPUTER"; break;    // My Computer
		case KC_MAIL        : getString = "MAIL"; break;    // Mail
		case KC_MEDIASELECT : getString = "MEDIA SELECT"; break;     // Media Select
	}
#else
    CGKeyCode deviceKeycode = 0;
    
    // Convert OIS KeyCode back into device keycode
    VirtualtoOIS_KeyMap keyMap = [mResponder keyConversionMap];
    for(VirtualtoOIS_KeyMap::iterator it = keyMap.begin(); it != keyMap.end(); ++it)
    {
        if(it->second == key)
            deviceKeycode = it->first;
    }
	
    UniChar unicodeString[1];
    UniCharCount actualStringLength;
	
    CGEventSourceRef sref = CGEventSourceCreate(kCGEventSourceStateHIDSystemState);
    CGEventRef ref = CGEventCreateKeyboardEvent(sref, deviceKeycode, true);
    CGEventKeyboardGetUnicodeString(ref, sizeof(unicodeString) / sizeof(*unicodeString), &actualStringLength, unicodeString);
    CFRelease(ref);
    CFRelease(sref);
    getString = unicodeString[0];
#endif
    return getString;
}

//-------------------------------------------------------------------//
void CocoaKeyboard::setBuffered( bool buffered )
{
	mBuffered = buffered;
}

//-------------------------------------------------------------------//
void CocoaKeyboard::copyKeyStates( char keys[256] ) const
{
	[mResponder copyKeyStates:keys];
}

@implementation CocoaKeyboardView

- (id)init
{
    self = [super init];
    if (self) {
		
        [self populateKeyConversion];
        memset( &KeyBuffer, 0, 256 );
        prevModMask = 0;
		autoGrabKeyboard = false;
		grabKeyboard = true;
		ignoreKeyEvents = false;
    }
    return self;
}

- (BOOL)acceptsFirstResponder
{
    return YES;
}

- (BOOL)canBecomeKeyView
{
    return YES;
}

- (void)setOISKeyboardObj:(CocoaKeyboard *)obj
{
    oisKeyboardObj = obj;
}

- (void)_grab:(bool)state
{
	if (state) {
		
	} else {
		
	}
}

- (void)setAutoGrab:(bool)state
{
	autoGrabKeyboard = state;
}

- (void)setGrabState:(bool)state
{
	grabKeyboard = state;
}

- (bool)getAutoGrabState
{
	return autoGrabKeyboard;
}

- (bool)getGrabState
{
	return grabKeyboard;
}


- (void)capture
{
	// If not buffered just return, we update the unbuffered automatically
	if ( !oisKeyboardObj->buffered() && !oisKeyboardObj->getEventCallback() )
		return;
	
	// Run through our event stack
	eventStack::iterator cur_it;
	
	for (cur_it = pendingEvents.begin(); cur_it != pendingEvents.end(); cur_it++)
	{
		if ( (*cur_it).type() == MAC_KEYDOWN || (*cur_it).type() == MAC_KEYREPEAT)
			oisKeyboardObj->propagateEvent(KE_Pressed, (*cur_it).event());
		else if ( (*cur_it).type() == MAC_KEYUP )
			oisKeyboardObj->propagateEvent(KE_Released, (*cur_it).event());
	}
	
	pendingEvents.clear();
}

- (void)setUseRepeat:(bool)repeat
{
    useRepeat = repeat;
}

- (bool)isKeyDown:(KeyCode)key
{
    return KeyBuffer[key];
}

- (void)copyKeyStates:(char [256])keys
{
	memcpy( keys, KeyBuffer, 256 );
}

- (void)populateKeyConversion
{
	// Virtual Key Map to KeyCode
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x12, KC_1));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x13, KC_2));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x14, KC_3));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x15, KC_4));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x17, KC_5));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x16, KC_6));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x1A, KC_7));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x1C, KC_8));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x19, KC_9));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x1D, KC_0));
	
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x33, KC_BACK));  // might be wrong
	
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x1B, KC_MINUS));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x18, KC_EQUALS));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x31, KC_SPACE));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x2B, KC_COMMA));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x2F, KC_PERIOD));
	
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x2A, KC_BACKSLASH));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x2C, KC_SLASH));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x21, KC_LBRACKET));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x1E, KC_RBRACKET));
	
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x35, KC_ESCAPE));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x39, KC_CAPITAL));
	
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x30, KC_TAB));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x24, KC_RETURN));  // double check return/enter
	
	//keyConversion.insert(VirtualtoOIS_KeyMap::value_type(XK_colon, KC_COLON));	 // no colon?
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x29, KC_SEMICOLON));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x27, KC_APOSTROPHE));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x32, KC_GRAVE));
	
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x0B, KC_B));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x00, KC_A));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x08, KC_C));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x02, KC_D));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x0E, KC_E));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x03, KC_F));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x05, KC_G));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x04, KC_H));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x22, KC_I));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x26, KC_J));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x28, KC_K));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x25, KC_L));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x2E, KC_M));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x2D, KC_N));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x1F, KC_O));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x23, KC_P));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x0C, KC_Q));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x0F, KC_R));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x01, KC_S));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x11, KC_T));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x20, KC_U));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x09, KC_V));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x0D, KC_W));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x07, KC_X));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x10, KC_Y));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x06, KC_Z));
	
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x7A, KC_F1));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x78, KC_F2));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x63, KC_F3));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x76, KC_F4));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x60, KC_F5));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x61, KC_F6));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x62, KC_F7));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x64, KC_F8));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x65, KC_F9));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x6D, KC_F10));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x67, KC_F11));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x6F, KC_F12));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x69, KC_F13));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x6B, KC_F14));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x71, KC_F15));
	
	// Keypad
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x52, KC_NUMPAD0));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x53, KC_NUMPAD1));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x54, KC_NUMPAD2));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x55, KC_NUMPAD3));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x56, KC_NUMPAD4));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x57, KC_NUMPAD5));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x58, KC_NUMPAD6));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x59, KC_NUMPAD7));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x5B, KC_NUMPAD8));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x5C, KC_NUMPAD9));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x45, KC_ADD));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x4E, KC_SUBTRACT));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x41, KC_DECIMAL));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x51, KC_NUMPADEQUALS));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x4B, KC_DIVIDE));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x43, KC_MULTIPLY));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x4C, KC_NUMPADENTER));
	
	// Keypad with numlock off
	//keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x73, KC_NUMPAD7));  // not sure of these
	//keyConversion.insert(VirtualtoOIS_KeyMap::value_type(XK_KP_Up, KC_NUMPAD8)); // check on a non-laptop
	//keyConversion.insert(VirtualtoOIS_KeyMap::value_type(XK_KP_Page_Up, KC_NUMPAD9));
	//keyConversion.insert(VirtualtoOIS_KeyMap::value_type(XK_KP_Left, KC_NUMPAD4));
	//keyConversion.insert(VirtualtoOIS_KeyMap::value_type(XK_KP_Begin, KC_NUMPAD5));
	//keyConversion.insert(VirtualtoOIS_KeyMap::value_type(XK_KP_Right, KC_NUMPAD6));
	//keyConversion.insert(VirtualtoOIS_KeyMap::value_type(XK_KP_End, KC_NUMPAD1));
	//keyConversion.insert(VirtualtoOIS_KeyMap::value_type(XK_KP_Down, KC_NUMPAD2));
	//keyConversion.insert(VirtualtoOIS_KeyMap::value_type(XK_KP_Page_Down, KC_NUMPAD3));
	//keyConversion.insert(VirtualtoOIS_KeyMap::value_type(XK_KP_Insert, KC_NUMPAD0));
	//keyConversion.insert(VirtualtoOIS_KeyMap::value_type(XK_KP_Delete, KC_DECIMAL));
	
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x7E, KC_UP));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x7D, KC_DOWN));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x7B, KC_LEFT));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x7C, KC_RIGHT));
	
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x74, KC_PGUP));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x79, KC_PGDOWN));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x73, KC_HOME));
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x77, KC_END));
	
	//keyConversion.insert(VirtualtoOIS_KeyMap::value_type(XK_Print, KC_SYSRQ));		// ??
	//keyConversion.insert(VirtualtoOIS_KeyMap::value_type(XK_Scroll_Lock, KC_SCROLL)); // ??
	//keyConversion.insert(VirtualtoOIS_KeyMap::value_type(XK_Pause, KC_PAUSE));		// ??
	
	
	//keyConversion.insert(VirtualtoOIS_KeyMap::value_type(XK_Insert, KC_INSERT));	  // ??
	keyConversion.insert(VirtualtoOIS_KeyMap::value_type(0x75, KC_DELETE)); // del under help key?
}

- (void)injectEvent:(KeyCode)kc eventTime:(unsigned int)time eventType:(MacEventType)type
{
    [self injectEvent:kc eventTime:time eventType:type eventText:0];
}

- (void)injectEvent:(KeyCode)kc eventTime:(unsigned int)time eventType:(MacEventType)type eventText:(unsigned int)txt
{
	// set to 1 if this is either a keydown or repeat
	KeyBuffer[kc] = ( type == MAC_KEYUP ) ? 0 : 1;
	
	if ( oisKeyboardObj->buffered() && oisKeyboardObj->getEventCallback() )
		pendingEvents.push_back( CocoaKeyStackEvent( KeyEvent(oisKeyboardObj, kc, txt), type) );
}


#pragma mark Mouse Event overrides
- (void)mouseEntered:(NSEvent *)theEvent
{
	std::cout << "[KB] Mouse entered window."<<std::endl;

	if (autoGrabKeyboard) {
		grabKeyboard = true;
		[self _grab:true];
		ignoreKeyEvents = false;
	}
}

- (void)mouseExited:(NSEvent *)theEvent
{
	std::cout << "[KB] Mouse left window."<<std::endl;
	if (autoGrabKeyboard) {
		grabKeyboard = false;
		[self _grab:false];
		ignoreKeyEvents = true;
	}
}

#pragma mark Key Event overrides
- (void)keyDown:(NSEvent *)theEvent
{
	unsigned short virtualKey = [theEvent keyCode];
	unsigned int time = (unsigned int)[theEvent timestamp];
	KeyCode kc = keyConversion[virtualKey];
	
	// Record what kind of text we should pass the KeyEvent
	unichar text[10];
	char macChar;
	if (oisKeyboardObj->getTextTranslation() == OIS::Keyboard::Unicode)
	{
		// Get string size
		NSUInteger stringsize = [[theEvent charactersIgnoringModifiers] length];
        [[theEvent charactersIgnoringModifiers] getCharacters:text range:NSMakeRange(0, stringsize)];
		//		NSLog(@"Characters: %ls", text);
		//		std::cout << "String length: " << stringsize << std::endl;
		
		if(stringsize > 0)
		{
			// For each unicode char, send an event
			for ( unsigned int i = 0; i < stringsize; i++ )
			{
                [self injectEvent:kc eventTime:time eventType:MAC_KEYDOWN eventText:(unsigned int)text[i]];
			}
		}
	}
	else if (oisKeyboardObj->getTextTranslation() == OIS::Keyboard::Ascii)
	{
        macChar = [[theEvent charactersIgnoringModifiers] characterAtIndex:0];
		[self injectEvent:kc eventTime:time eventType:MAC_KEYDOWN eventText:(unsigned int)macChar];
	}
	else
	{
		[self injectEvent:kc eventTime:time eventType:MAC_KEYDOWN];
	}
}

- (void)keyUp:(NSEvent *)theEvent
{
    unsigned short virtualKey = [theEvent keyCode];
	
	KeyCode kc = keyConversion[virtualKey];
    [self injectEvent:kc eventTime:[theEvent timestamp] eventType:MAC_KEYUP];
}

- (void)flagsChanged:(NSEvent *)theEvent
{
	NSUInteger mods = [theEvent modifierFlags];
	
	// Find the changed bit
	NSUInteger change = prevModMask ^ mods;
	MacEventType newstate = ((change & prevModMask) > 0) ? MAC_KEYUP : MAC_KEYDOWN;
	unsigned int time = (unsigned int)[theEvent timestamp];
    unsigned int modsRef = oisKeyboardObj->_getModifiers();
	//    cout << "Key " << ((newstate == MAC_KEYDOWN) ? "Down" : "Up") << endl;
	//    cout << "preMask: 0x" << hex << prevModMask << endl;
	//    cout << "ModMask: 0x" << hex << mods << endl;
	//    cout << "Change:  0x" << hex << (change & prevModMask) << endl << endl;
	
	//    cout << "Modifiers before: 0x" << hex << modsRef << endl;
    
	if (change & NSAlphaShiftKeyMask) { // CAPS_LOCK
		[self injectEvent:KC_CAPITAL eventTime:time eventType:newstate];
	}
	if (change & NSShiftKeyMask) { // SHIFT
		if (newstate==MAC_KEYDOWN) modsRef |= OIS::Keyboard::Shift; else modsRef &= ~OIS::Keyboard::Shift;
		oisKeyboardObj->_getModifiers() &= (newstate == MAC_KEYDOWN) ? OIS::Keyboard::Shift : -OIS::Keyboard::Shift;
		if (change & NX_DEVICELSHIFTKEYMASK) {
			[self injectEvent:KC_LSHIFT eventTime:time eventType:newstate];
		}
		if (change & NX_DEVICERSHIFTKEYMASK) {
			[self injectEvent:KC_RSHIFT eventTime:time eventType:newstate];
		}
	}
	if (change & NSAlternateKeyMask) { // ALT
		if (newstate==MAC_KEYDOWN) modsRef |= OIS::Keyboard::Alt; else modsRef &= ~OIS::Keyboard::Alt;
		oisKeyboardObj->_getModifiers() &= (newstate == MAC_KEYDOWN) ? OIS::Keyboard::Alt : -OIS::Keyboard::Alt;
		if (change & NX_DEVICELALTKEYMASK) {
			[self injectEvent:KC_LMENU eventTime:time eventType:newstate];
		}
		if (change & NX_DEVICERALTKEYMASK) {
			[self injectEvent:KC_RMENU eventTime:time eventType:newstate];
		}
	}
	if (change & NSControlKeyMask) { // CTRL
		if (newstate==MAC_KEYDOWN) modsRef |= OIS::Keyboard::Ctrl; else modsRef &= ~OIS::Keyboard::Ctrl;
		oisKeyboardObj->_getModifiers() += (newstate == MAC_KEYDOWN) ? OIS::Keyboard::Ctrl : -OIS::Keyboard::Ctrl;
		if (change & NX_DEVICELCTLKEYMASK) {
			[self injectEvent:KC_LCONTROL eventTime:time eventType:newstate];
		}
		if (change & NX_DEVICERCTLKEYMASK) {
			[self injectEvent:KC_RCONTROL eventTime:time eventType:newstate];
		}
	}
	if (change & NSCommandKeyMask) { // WIN/SUPER/APPLE
		[self injectEvent:KC_LWIN eventTime:time eventType:newstate];
	}
	if (change & NSFunctionKeyMask) {
		[self injectEvent:KC_APPS eventTime:time eventType:newstate];
	}
	
	if([theEvent keyCode] == NSClearLineFunctionKey) // numlock
        [self injectEvent:KC_NUMLOCK eventTime:time eventType:newstate];
	
	//    cout << "Modifiers after: 0x" << hex << modsRef << endl;
    oisKeyboardObj->_setModifiers(modsRef);
	prevModMask = mods;
}

- (VirtualtoOIS_KeyMap)keyConversionMap
{
    return keyConversion;
}

@end
