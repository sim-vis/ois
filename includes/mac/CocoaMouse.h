/*
 The zlib/libpng License
 
 Copyright (c) 2005-2007 Phillip Castaneda (pjcast -- www.wreckedgames.com)
 
 This software is provided 'as-is', without any express or implied warranty. In no event will
 the authors be held liable for any damages arising from the use of this software.
 
 Permission is granted to anyone to use this software for any purpose, including commercial
 applications, and to alter it and redistribute it freely, subject to the following
 restrictions:
 
 1. The origin of this software must not be misrepresented; you must not claim that
 you wrote the original software. If you use this software in a product,
 an acknowledgment in the product documentation would be appreciated but is
 not required.
 
 2. Altered source versions must be plainly marked as such, and must not be
 misrepresented as being the original software.
 
 3. This notice may not be removed or altered from any source distribution.
 */
#ifndef OIS_CocoaMouse_H
#define OIS_CocoaMouse_H

#include "OISMouse.h"
#include "mac/CocoaHelpers.h"

#include <Cocoa/Cocoa.h>

@class CocoaMouseView;

using namespace OIS;

namespace OIS
{
	class CocoaMouse : public Mouse
    {
	public:
		CocoaMouse( InputManager* creator, bool buffered );
		virtual ~CocoaMouse();
		
		/** @copydoc Object::setBuffered */
		virtual void setBuffered(bool buffered);

		/** @copydoc Object::capture */
		virtual void capture();

		/** @copydoc Object::queryInterface */
		virtual Interface* queryInterface(Interface::IType type) {return 0;}

		/** @copydoc Object::_initialize */
		virtual void _initialize();
        
        MouseState * getMouseStatePtr() { return &(mState); }

		/**
		 * Method that indicates whether or not a specific provider supports mouse grabbing.
		 * @return True if the provider supports mouse grabbing, false otherwise
		 */
		virtual bool supportsMouseGrabbing(void) { return true; }

		/**
		 * Method that indicates whether or not a specific provider supports changing the mouse visibility.
		 * @return True if the provider supports changing the mouse visibility, false otherwise
		 */
		virtual bool supportsMouseVisibility(void) { return true; }
		
		/**
		 * Method that indicates whether or not a specific provider supports sending messages upon mouse entering/exiting tracking window.
		 * @return True if the provider supports entering/exiting tracking, false otherwise
		 */
		virtual bool supportsMouseAcquire(void) { return true; }

		/**
		 * @remarks
		 * Sets the visibility state of the OS mouse cursor.
		 * @param state True if the OS mouse cursor should be visible, false otherwise
		 */
		virtual void setVisibility(bool state);

		/**
		 * @remarks
		 * Gets the current visibility state of the OS mouse cursor
		 * @return True if the OS mouse cursor is visible, false otherwise
		 */
		virtual bool getVisibility(void);

		/**
		 * @remarks
		 * Hides the OS mouse cursor
		 * @param state True if the cursor shoudl be hidden, false otherwise
		 */
		virtual void hide(bool state);

		/**
		 * @remarks
		 * Sets the mouse grab state.
		 * @param state True if the mouse should be grabbed by the window, false otherwise
		 */
		virtual void setMouseGrab(bool state);

		/**
		 * @remarks
		 * Gets the mouse grab state.
		 * @return True if the mouse is grabbed by the window, false otherwise
		 */
		virtual bool getMouseGrab(void);
		
		
		/**
		 * @remarks
		 * Sets automatic mouse grab upon mouse button press and disables upon mouse button release.
		 * @param state True if the auto grab feature should be active, false otherwise
		 */
		virtual void setMouseAutoGrab(bool state);
		
		/**
		 * @remarks
		 * Gets the automatic mouse grab state.
		 * @return True if the auto grab feature is active false otherwise
		 */
		virtual bool getMouseAutoGrab(void);

		/**
		 * @remarks
		 * Grabs the mouse
		 * @param state True if the mouse should be grabbed by the window, false otherwise
		 */
		virtual void grab(bool state);

	protected:
        CocoaMouseView *mResponder;
	};
}

@interface CocoaMouseView : NSView
{
    NSTrackingArea *mTrackingArea;
    CocoaMouse *oisMouseObj;
    MouseState mTempState;
    bool mNeedsToRegainFocus;
    bool mMouseWarped;
	bool mMoved;
	bool grabMouse;		//Are we grabbing the mouse to the window?
	bool hideMouse;		//Are we hiding OS mouse?
	bool autoGrab;		//Automatic grab
	bool osCursorVisible;
	bool inited;		// whether or not we have been initialized
	int mouseCounter;
	NSPoint oldMousePosition;
}

- (void)setOISMouseObj:(CocoaMouse *)obj;
- (void)capture;
- (void)hide:(bool)hide;
- (bool)isVisible;
- (void)setVisibility:(bool)state;
- (void)setMouseGrab:(bool)state;
- (void)setMouseAutoGrab:(bool)state;
- (bool)isAutoGrabbed;
- (void)grab:(bool)grab;
- (bool)isGrabbed;

- (void)viewDidMoveToWindow;
- (void)dealloc;
- (void)windowResized:(NSNotification *)notification;
- (void)updateTrackingAreas;

- (void)_mouseMoved:(NSEvent *)theEvent;

@end

#endif // OIS_CocoaMouse_H
