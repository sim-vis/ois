/*
The zlib/libpng License

Copyright (c) 2005-2007 Phillip Castaneda (pjcast -- www.wreckedgames.com)

This software is provided 'as-is', without any express or implied warranty. In no event will
the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose, including commercial
applications, and to alter it and redistribute it freely, subject to the following
restrictions:

    1. The origin of this software must not be misrepresented; you must not claim that
		you wrote the original software. If you use this software in a product,
		an acknowledgment in the product documentation would be appreciated but is
		not required.

    2. Altered source versions must be plainly marked as such, and must not be
		misrepresented as being the original software.

    3. This notice may not be removed or altered from any source distribution.
*/
#ifndef OIS_Mouse_H
#define OIS_Mouse_H
#include "OISObject.h"
#include "OISEvents.h"
#include <list>

namespace OIS
{
	//! Button ID for mouse devices
	enum MouseButtonID
	{
		MB_Left = 0, MB_Right, MB_Middle,
		MB_Button3, MB_Button4,	MB_Button5, MB_Button6,	MB_Button7
	};

	//! Mouse Event types
	enum MouseEventType
	{
		ME_Moved = 0, ME_Pressed, ME_Released,
		ME_Unacquired, ME_Acquired
	};

	/**
		Represents the state of the mouse
		All members are valid for both buffered and non buffered mode
	*/
	class _OISExport MouseState
	{
	public:
		MouseState() : width(50), height(50), buttons(0), inWindow(true) {};

		/** Represents the height/width of your display area.. used if mouse clipping
		or mouse grabbed in case of X11 - defaults to 50.. Make sure to set this
		and change when your size changes.. */
		mutable int width, height;

		//! X Axis component
		Axis X;

		//! Y Axis Component
		Axis Y;

		//! Z Axis Component
		Axis Z;

		//! represents all buttons - bit position indicates button down
		int buttons;

		//! Flag that indicates whether or not the mouse is in the window
		bool inWindow;

		//! Button down test
		inline bool buttonDown( MouseButtonID button ) const
		{
			return ((buttons & ( 1L << button )) == 0) ? false : true;
		}

		//! Clear all the values
		void clear()
		{
			X.clear();
			Y.clear();
			Z.clear();
			buttons = 0;
		}
	};

	/** Specialised for mouse events */
	class _OISExport MouseEvent : public EventArg
	{
	public:
		MouseEvent( Object *obj, const MouseState &ms )	: EventArg(obj), state(ms) {}
		virtual ~MouseEvent() {}

		//! The state of the mouse - including buttons and axes
		const MouseState &state;
	};

	/**
		To receive buffered mouse input, derive a class from this, and implement the
		methods here. Then set the call back to your Mouse instance with Mouse::setEventCallback
	*/
	class _OISExport MouseListener
	{
	public:
		virtual ~MouseListener() {}
		virtual bool mouseMoved( const MouseEvent &arg ) = 0;
		virtual bool mousePressed( const MouseEvent &arg, MouseButtonID id ) = 0;
		virtual bool mouseReleased( const MouseEvent &arg, MouseButtonID id ) = 0;
		virtual bool mouseUnacquired( const MouseEvent &arg) { return true; };
		virtual bool mouseAcquired( const MouseEvent &arg) { return true; };
	};

	/**
		Mouse base class. To be implemented by specific system (ie. DirectX Mouse)
		This class is useful as you remain OS independent using this common interface.
	*/
	class _OISExport Mouse : public Object
	{
	public:
		virtual ~Mouse() {}

		/**
		 * @remarks
		 * Sets the visibility state of the OS mouse cursor.
		 * @param state True if the OS mouse cursor should be visible, false otherwise
		 */
		virtual void setVisibility(bool state) {}

		/**
		 * @remarks
		 * Gets the current visibility state of the OS mouse cursor
		 * @return True if the OS mouse cursor is visible, false otherwise
		 */
		virtual bool getVisibility(void) { return false; }

		/**
		 * @remarks
		 * Sets the mouse grab state.
		 * @param state True if the mouse should be grabbed by the window, false otherwise
		 */
		virtual void setMouseGrab(bool state) {}

		/**
		 * @remarks
		 * Gets the mouse grab state.
		 * @return True if the mouse is grabbed by the window, false otherwise
		 */
		virtual bool getMouseGrab(void) { return false; }
		
		/**
		 * @remarks
		 * Sets automatic mouse grab upon mouse button press and disables upon mouse button release.
		 * @param state True if the auto grab feature should be active, false otherwise
		 */
		virtual void setMouseAutoGrab(bool state) {}
		
		/**
		 * @remarks
		 * Gets the automatic mouse grab state.
		 * @return True if the auto grab feature is active false otherwise
		 */
		virtual bool getMouseAutoGrab(void) { return false; }

		/**
		@remarks
			Register/unregister a Mouse Listener - Only one allowed for simplicity. If broadcasting
			is neccessary, just broadcast from the callback you registered.
		@param mouseListener
			Send a pointer to a class derived from MouseListener or 0 to clear the callback
		*/
		virtual void setEventCallback( MouseListener *mouseListener ) { addEventCallback(mouseListener); }

		/**
		 * @remarks
		 * Registers a new Mouse listener, if there is no registered mouse listener this has the same effect
		 * as the previous version.
		 * @param mouseListener
		 *   Send a pointer to a class derived from MouseListener or 0 to clear the callback
		 */
		virtual void addEventCallback( MouseListener *mouseListener ) {
			if (mouseListener == NULL) {
				removeEventCallback(mouseListener);
				return; // method will figure out what to do
			}
			std::list<MouseListener *>::iterator it = mListeners.begin();
			while(it != mListeners.end()) {
				if (*it == mouseListener) return; // can't add an already added listener
				it++;
			}
			mListeners.push_back(mouseListener);
			if (mListener == NULL)
				mListener = mouseListener;
		}

		/**
		 * @remarks
		 * Method that removes a Mouse Listener from the list of registered listeners.
		 * @param mouseListener Listener to remove
		 */
		virtual void removeEventCallback( MouseListener *mouseListener ) {
			if (mouseListener == NULL && mListeners.size() == 1) {
				mListeners.clear();
				mListener = NULL;
				return;
			}
			std::list<MouseListener *>::iterator it = mListeners.begin();
			while(it != mListeners.end()) {
				if (*it == mouseListener) {
					mListeners.erase(it);
					break;
				}
				it++;
			}
			if (mListener == mouseListener)
				mListener = NULL;
			if (mListeners.size() > 0)
				mListener = mListeners.front();
		}

		/**
		 * Method that indicates whether or not a specific provider supports mouse grabbing.
		 * @return True if the provider supports mouse grabbing, false otherwise
		 */
		virtual bool supportsMouseGrabbing(void) { return false; }

		/**
		 * Method that indicates whether or not a specific provider supports changing the mouse visibility.
		 * @return True if the provider supports changing the mouse visibility, false otherwise
		 */
		virtual bool supportsMouseVisibility(void) { return false; }
		
		/**
		 * Method that indicates whether or not a specific provider supports sending messages upon mouse entering/exiting tracking window.
		 * @return True if the provider supports entering/exiting tracking, false otherwise
		 */
		virtual bool supportsMouseAcquire(void) { return false; }

		/** @remarks Returns currently set callback.. or 0 */
		MouseListener* getEventCallback() const { return mListener; }

		/**
		 * @remarks Returns a list of currently registered callbacks.
		 */
		std::list<MouseListener*> getEventCallbacks() const { return mListeners; }

		/** @remarks Returns the state of the mouse - is valid for both buffered and non buffered mode */
		const MouseState& getMouseState() const { return mState; }

		/**
		 * @remarks Method that propagates an event to all registered receivers
		 * @param type Event type
		 * @param arg MouseEvent argument
		 * @param id Mouse Button id (required only for some events)
		 * @return True if all handlers returned true, false otherwise
		 */
		virtual bool propagateEvent( const MouseEventType type, const MouseEvent &arg, MouseButtonID id = (MouseButtonID)0)
		{
			if (mListeners.empty()) return true; // nothing to do
			std::list<MouseListener *>::iterator it = mListeners.begin();
			MouseListener * listener = NULL;
			bool ret = true;
			while (it != mListeners.end()) {
				listener = (*it);
				switch(type) {
					case ME_Moved: if (listener->mouseMoved(arg) == false) ret = false; break;
					case ME_Pressed: if (listener->mousePressed(arg,id) == false) ret = false; break;
					case ME_Released: if (listener->mouseReleased(arg,id) == false) ret = false; break;
					case ME_Acquired: if (listener->mouseAcquired(arg) == false) ret = false; break;
					case ME_Unacquired: if (listener->mouseUnacquired(arg) == false) ret = false; break;
					default: break;
				}
				it++;
			}
			return ret;
		}

	protected:
		Mouse(const std::string &inVendor, bool inBuffered, int devID, InputManager* creator)
			: Object(inVendor, OISMouse, inBuffered, devID, creator), mListener(0) {}

		//! The state of the mouse
		MouseState mState;

		//! Default mouse listener, used for buffered/action mapping callback
		MouseListener * mListener;

		//! List of all registered mouse listeners, used for buffered/action mapping callback
		std::list<MouseListener *> mListeners;
	};
}
#endif

